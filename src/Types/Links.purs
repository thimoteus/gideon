module Types.Links where

import Prelude
import Util (httprefix)

import Data.Argonaut.Core (jsonEmptyObject)
import Data.Argonaut.Encode (class EncodeJson, (:=), (~>), encodeJson)
import Data.Argonaut.Decode (class DecodeJson, decodeJson, (.?))
import Data.DateTime (DateTime)
import Data.JSDate (JSDate, toDateTime)
import Data.Lens (lens)
import Data.Lens.Types (Lens')
import Data.Maybe (Maybe(..))
import Network.HTTP.Affjax.Request (class Requestable, toRequest)

foreign import toJSDate :: Number -> JSDate

type Tag = String

newtype Link
  = Link
  { url :: String
  , date :: Maybe DateTime
  , tags :: Array Tag
  , currTag :: String
  , archive :: String
}

derive instance eqLink :: Eq Link

emptyLink :: Link
emptyLink = Link {url: "", date: Nothing, tags: [], currTag: "", archive: ""}

isEmpty :: Link -> Boolean
isEmpty = eq emptyLink

-- we want the reverse ordering so more recent links get sorted near the
-- beginning of an array
instance ordLink :: Ord Link where
  compare (Link {date: d1}) (Link {date: d2}) = compare d2 d1

instance decodeLink :: DecodeJson Link where
  decodeJson json = do
    obj <- decodeJson json
    url <- obj .? "url"
    date <- toDateTime <<< toJSDate <<< (_ * 1000.0) <$> obj .? "date"
    tags <- obj .? "tags"
    archive <- obj .? "archive"
    pure $ Link {url, date, tags, currTag: "", archive}

instance encodeLink :: EncodeJson Link where
  encodeJson (Link {url, tags, archive}) =
    "url" := httprefix url ~>
    "tags" := tags ~>
    "archive" := archive ~>
    jsonEmptyObject

instance requestableLink :: Requestable Link where
  toRequest = toRequest <<< encodeJson

_url :: Lens' Link String
_url = lens g s
  where
    g (Link {url}) = url
    s (Link l) u = Link l {url = u}

_date :: Lens' Link (Maybe DateTime)
_date = lens g s
  where
    g (Link {date}) = date
    s (Link l) d = Link l {date = d}

_tags :: Lens' Link (Array String)
_tags = lens g s
  where
    g (Link {tags}) = tags
    s (Link l) ts = Link l {tags = ts}

_currTag :: Lens' Link String
_currTag = lens g s
  where
    g (Link {currTag}) = currTag
    s (Link l) t = Link l {currTag = t}

_archive :: Lens' Link String
_archive = lens g s
  where
    g (Link {archive}) = archive
    s (Link l) a = Link l {archive = a}

