module Types.Query where

import Types.Links (Link)
import Loaded (IsLoaded)

import Pux.DOM.Events (DOMEvent)

data Query
  = Fail Component String
  | Success Component String
  | Submit Component
  | KeyPress Component DOMEvent
  -- Links
  | GetLinks (Array Link)
  | AddTag
  | UrlChange DOMEvent
  | TagChange DOMEvent
  -- Auth
  | NameChange DOMEvent
  | EmailChange Component DOMEvent
  | PasswordChange Component DOMEvent
  | RememberMe
  -- Filtering
  | RemoveFilter
  | NegateFilter
  | ChangeLit DOMEvent
  | AddLit
  | AddConj
  -- Loaded
  | IsLoaded IsLoaded

data Component
  = Register
  | Login
  | Logout
  | Links
  | NewLink
  | NLTag
  | Tag
