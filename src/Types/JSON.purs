module Types.JSON where

import Prelude
import Data.Argonaut.Decode (class DecodeJson, decodeJson, (.?))
import Data.Argonaut.Decode.Combinators ((.??))
import Data.Maybe (Maybe(..))

data JsonMsgResponse = OK String | Err String

instance decodeJsonMsgResponse :: DecodeJson JsonMsgResponse where
  decodeJson json = do
    obj <- decodeJson json
    msg <- obj .?? "msg"
    case msg of
         Just str -> pure $ OK str
         _ -> do
           err <- obj .? "err"
           pure $ Err err
