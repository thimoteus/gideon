module Types.State where

import Prelude
import Browser.WebStorage (WebStorage)
import Data.Pair (Pair, pair)
import Data.Proposition (Proposition, emptyProp)
import Types.Links (Link, Tag)
import Loaded (IsLoaded(..))
import Util (StatusText, good, poor)

import Control.Monad.Eff.Console (CONSOLE)
import Control.Monad.Eff.Ref (REF)
import Data.Argonaut.Core (jsonEmptyObject)
import Data.Argonaut.Encode (class EncodeJson, encodeJson, (:=), (~>))
import Data.Lens (Lens', lens, _1, _2)
import Data.Maybe (Maybe(..))
import Data.Monoid (class Monoid)
import Data.String (null)
import DOM (DOM)
import DOM.HTML.Types (HISTORY)
import Network.HTTP.Affjax (AJAX)
import Network.HTTP.Affjax.Request (class Requestable, toRequest)
import Pux (CoreEffects)

type State =
  { links :: Array Link
  , newLink :: Maybe Link
  , authstate :: Pair AuthState
  , token :: Maybe String
  , rememberMe :: Boolean
  , status :: StatusText
  , loaded :: IsLoaded
  , filter ::
    { tags :: Maybe (Array Tag)
    , prop :: Proposition Tag
    , currTag :: Tag
    , negate :: Boolean
    }
}

type AppEff = CoreEffects StateEff

type StateEff =
  ( ajax :: AJAX
  , console :: CONSOLE
  , webStorage :: WebStorage
  , ref :: REF
  , history :: HISTORY
  , dom :: DOM
  )

initState :: {token :: Maybe String, rememberMe :: Boolean} -> State
initState {token, rememberMe} =
  { newLink: Nothing
  , authstate: pair initAuthState
  , links : []
  , filter: initFilter
  , status: good "Welcome to Waverider"
  , loaded: NotLoaded
  , token
  , rememberMe
}
  where
    initFilter =
      { tags: Nothing
      , prop: emptyProp
      , currTag: ""
      , negate: false
    }

newtype AuthState = AuthState
  { username :: String
  , email :: String
  , password :: String
}

instance encodeAuthState :: EncodeJson AuthState where
  encodeJson (AuthState as)
    = "username" := as.username
    ~> "email" := as.email
    ~> "password" := as.password
    ~> jsonEmptyObject

instance requestableAuthState :: Requestable AuthState where
  toRequest = toRequest <<< encodeJson

instance semigroupAuthState :: Semigroup AuthState where
  append (AuthState s1) (AuthState s2) = AuthState if f s1 then s2 else s1
    where
      f {email, password} = null email || null password

instance monoidAuthState :: Monoid AuthState where
  mempty = AuthState {email: "", username: "", password: ""}

_username :: Lens' AuthState String
_username = lens g s
  where
    g (AuthState {username}) = username
    s (AuthState as) u = AuthState as {username = u}

_email :: Lens' AuthState String
_email = lens g s
  where
    g (AuthState {email}) = email
    s (AuthState as) e = AuthState as {email = e}

_password :: Lens' AuthState String
_password = lens g s
  where
    g (AuthState {password}) = password
    s (AuthState as) p = AuthState as {password = p}

_register :: Lens' State AuthState
_register = _r <<< _1
  where
    _r = lens _.authstate (_ {authstate = _})

_login :: Lens' State AuthState
_login = _l <<< _2
  where
    _l = lens _.authstate (_ {authstate = _})

initAuthState :: AuthState
initAuthState = AuthState {username: "", email: "", password: ""}

impossible :: String -> State -> State
impossible msg state = state { status = poor $ "The impossible happened: " <> msg }
