module Loaded where

import Prelude

import Data.Maybe (fromMaybe)
import Pux.Router (router, end)

data IsLoaded
  = Loaded
  | NotLoaded

match :: String -> IsLoaded
match url = fromMaybe NotLoaded $ router url $ Loaded <$ end
