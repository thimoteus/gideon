module Update.Links where

import Prelude

import Types.Query (Query(..), Component(..))
import Types.JSON (JsonMsgResponse(..))
import Types.State (State, StateEff, impossible)
import Types.Links (Link, _url, _currTag, _tags, _date, emptyLink, isEmpty)
import Request (post, get)
import Util (purem, good, medium)

import Control.Monad.Aff (Aff)
import Data.Argonaut.Decode.Class (decodeJson)
import Data.Array ((:), nub, sort)
import Data.Either (Either(..))
import Data.Lens (Lens')
import Data.Lens.Getter ((^.))
import Data.Lens.Setter ((.~), (<>~), (%~))
import Data.Maybe (Maybe(..), fromMaybe)
import Data.String (null, take, length)
import Network.HTTP.Affjax (AJAX)
import Network.HTTP.StatusCode (StatusCode(..))
import Pux (EffModel, noEffects)
import Pux.DOM.Events (DOMEvent, targetValue)

urlChange :: DOMEvent -> State -> EffModel State Query StateEff
urlChange ev = noEffects <<< change _url (targetValue ev)

tagChange :: DOMEvent -> State -> EffModel State Query StateEff
tagChange ev = noEffects <<< change _currTag (targetValue ev)

change :: Lens' Link String -> String -> State -> State
change l val state =
  let link = l .~ val $ fromMaybe emptyLink state.newLink
   in state { newLink = if isEmpty link then Nothing else Just link }

addTag :: State -> EffModel State Query StateEff
addTag state@{newLink: Nothing} = noEffects state
addTag state@{newLink: Just link} =
  let tag' = link ^. _currTag
      tag = take (length tag' - 1) tag'
      updateTags = (_currTag .~ "") <<< (_tags %~ nub) <<< (_tags <>~ [tag])
   in noEffects if null tag then state else state { newLink = Just $ updateTags link }

submitNewLink :: State -> EffModel State Query StateEff
submitNewLink state =
  { state: state { status = medium "Submitting link ... " }
  , effects: [submit state]
}

submit :: forall eff. State -> Aff (ajax :: AJAX | eff) (Maybe Query)
submit { newLink: Nothing } = purem $ Fail NewLink "Error: Trying to submit an empty link"
submit state@{ newLink: Just link } = do
  res <- post state "/submit" link
  case decodeJson res.response of
       Right (OK msg) -> purem $ Success NewLink msg
       Right (Err err) -> purem $ Fail NewLink err
       Left err -> purem $ Fail NewLink err

getLinks :: Array Link -> State -> EffModel State Query StateEff
getLinks xs = noEffects <<< _ { links = xs, status = good "Links retrieved" }

-- TODO: deal with case where response could be a jsonErr instead of [Link]
requestLinks :: State -> EffModel State Query StateEff
requestLinks state =
  { state: state { status = medium "Requesting links ... " }
  , effects: [ do
      res <- get state "/links.json"
      case res.status of
           StatusCode 401 -> purem $ Submit Logout
           _ -> case decodeJson res.response of
                     Right links -> purem $ GetLinks $ sort links
                     Left err -> purem $ Fail Links err
    ]
  }

addNewLinkToLinks :: String -> State -> State
addNewLinkToLinks msg state@{ newLink: Just link, links } =
  let upDate = _date .~ Nothing
   in state { newLink = Nothing, links = upDate link : state.links, status = good msg }
addNewLinkToLinks _ state = impossible "Somehow submitted an empty link" state
