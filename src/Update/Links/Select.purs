module Update.Links.Select where

import Prelude

import Data.Proposition (Proposition, Disj(..), Conj(..), Lit(..), emptyProp)
import Types.State (State, StateEff)
import Types.Query (Query)

import Data.Maybe (Maybe(..))
import Data.List (List(..), (:))
import Pux (EffModel, noEffects)
import Pux.DOM.Events (DOMEvent, targetValue)

removeFilter :: State -> EffModel State Query StateEff
removeFilter state = noEffects $
  state
    { filter
      { tags = Nothing
      , prop = emptyProp :: Proposition String
      , currTag = ""
      , negate = false
      }
    }

negateFilter :: State -> EffModel State Query StateEff
negateFilter state = noEffects $
  state
    { filter
      { negate = not state.filter.negate
      }
    }

addLit :: State -> EffModel State Query StateEff
addLit state = noEffects $
  state
    { filter
      { prop = addLit' state.filter.negate state.filter.currTag state.filter.prop
      , currTag = ""
      }
    }

addLit' :: forall a. Boolean -> a -> Proposition a -> Proposition a
addLit' b lit (Disj (Conj lits : conjs)) =
  if b
     then Disj (Conj (Not lit : lits) : conjs)
     else Disj (Conj (Atom lit : lits) : conjs)
addLit' b lit _ =
  if b
     then Disj (Conj (Not lit : Nil) : Nil)
     else Disj (Conj (Atom lit : Nil) : Nil)

addConj :: State -> EffModel State Query StateEff
addConj state = noEffects $
  state
    { filter
      { prop = addConj' state.filter.prop
      }
    }

addConj' :: forall a. Proposition a -> Proposition a
addConj' (Disj cs) = Disj (Conj Nil : cs)

changeLit :: DOMEvent -> State -> EffModel State Query StateEff
changeLit ev state = noEffects $
  state
    { filter
      { currTag = targetValue ev
      }
    }
