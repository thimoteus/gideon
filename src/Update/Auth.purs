module Update.Auth where

import Prelude
import Types.Query (Query(..), Component(..))
import Types.JSON (JsonMsgResponse(..))
import Types.State (StateEff, State, _username, _register, _login, initState)
import Util (purem, medium)

import Control.Monad.Eff.Class (liftEff)
import Browser.WebStorage (removeItem, localStorage, sessionStorage)
import Data.Argonaut.Decode.Class (decodeJson)
import Data.Either (Either(..))
import Data.Lens ((^.), set)
import Data.Maybe (Maybe(..))
import Network.HTTP.Affjax (post)
import Pux (EffModel, noEffects)
import Pux.DOM.Events (DOMEvent, targetValue)

nameChange :: DOMEvent -> State -> EffModel State Query StateEff
nameChange ev = noEffects <<< set (_register <<< _username) (targetValue ev)

register :: State -> EffModel State Query StateEff
register state =
  { state: state { status = medium "Registering ... " }
  , effects: [ do
      res <- post "/register" $ state ^. _register
      case decodeJson res.response of
           Right (OK msg) -> purem $ Success Register msg
           Right (Err err) -> purem $ Fail Register err
           Left err -> purem $ Fail Register err
     ]
   }

login :: State -> EffModel State Query StateEff
login state = -- do
  { state: state { status = medium "Logging in ... " }
  , effects: [ do
      res <- post "/login" $ state ^. _login
      case decodeJson res.response of
           Right (OK msg) -> purem $ Success Login msg
           Right (Err err) -> purem $ Fail Login err
           Left err -> purem $ Fail Login err
    ]
  }

logout :: State -> EffModel State Query StateEff
logout state =
  let status = _.status $ initState {token: Nothing, rememberMe: false}
   in { state: state { token = Nothing, status = status, links = [] }
      , effects: [ Nothing <$ liftEff do
          removeItem localStorage "token"
          removeItem sessionStorage "token"
        ]
      }
