module Update.App where

import Prelude
import Loaded (IsLoaded(..))
import Types.Query (Query(..), Component(..))
import Types.State (State, StateEff, _register, _login, _password, _email)
import Update.Auth (nameChange, login, register, logout)
import Update.Links (getLinks, urlChange, tagChange, addTag, addNewLinkToLinks, submitNewLink, requestLinks)
import Update.Links.Select (removeFilter, negateFilter, addLit, addConj, changeLit)
import Util (getKeycode, purem, good, poor)

import Browser.WebStorage (class Storage, WebStorage, localStorage, sessionStorage, setItem)
import Control.Monad.Aff (Aff)
import Control.Monad.Eff.Class (liftEff)
import Data.Maybe (Maybe(..))
import Data.Lens (set)
import Pux (EffModel, noEffects, onlyEffects)
import Pux.DOM.Events (DOMEvent, targetValue)

foldp :: Query -> State -> EffModel State Query StateEff
foldp (Fail comp msg) = fail comp msg
foldp (Success comp msg) = success comp msg
foldp (Submit comp) = submit comp
foldp (KeyPress comp ev) = keyPress comp ev
-- Links
foldp (GetLinks xs) = getLinks xs
foldp AddTag = addTag
foldp (UrlChange ev) = urlChange ev
foldp (TagChange ev) = tagChange ev
-- Auth
foldp (NameChange ev) = nameChange ev
foldp (EmailChange comp ev) = emailChange comp ev
foldp (PasswordChange comp ev) = passwordChange comp ev
foldp RememberMe = rememberMe
-- Filtering
foldp RemoveFilter = removeFilter
foldp NegateFilter = negateFilter
foldp AddLit = addLit
foldp (ChangeLit ev) = changeLit ev
foldp AddConj = addConj
-- Loaded
foldp (IsLoaded loaded) = isLoaded loaded

fail :: Component -> String -> State -> EffModel State Query StateEff
fail _ msg = noEffects <<< _ { status = poor msg }

success :: Component -> String -> State -> EffModel State Query StateEff
success comp msg =
  case comp of
       NewLink -> noEffects <<< addNewLinkToLinks msg
       Login -> showLinks msg
       Register -> noEffects <<< _ { status = good msg }
       _ -> noEffects

submit :: Component -> State -> EffModel State Query StateEff
submit =
  case _ of
       NewLink -> submitNewLink
       Links -> requestLinks
       Login -> login
       Register -> register
       Logout -> logout
       _ -> noEffects

keyPress :: Component -> DOMEvent -> State -> EffModel State Query StateEff
keyPress comp ev =
  case comp, getKeycode ev of
       Register, "Enter" -> flip onlyEffects [purem $ Submit Register]
       Login, "Enter" -> flip onlyEffects [purem $ Submit Login]
       NewLink, "Enter" -> flip onlyEffects [purem $ Submit NewLink]
       NLTag, "Enter" -> flip onlyEffects [purem $ Submit NewLink]
       NLTag, "," -> flip onlyEffects [purem $ AddTag]
       _, _ -> noEffects

emailChange :: Component -> DOMEvent -> State -> EffModel State Query StateEff
emailChange comp ev =
  case comp of
       Register -> noEffects <<< set (_register <<< _email) (targetValue ev)
       Login -> noEffects <<< set (_login <<< _email) (targetValue ev)
       _ -> noEffects

passwordChange :: Component -> DOMEvent -> State -> EffModel State Query StateEff
passwordChange comp ev =
  case comp of
       Register -> noEffects <<< set (_register <<< _password) (targetValue ev)
       Login -> noEffects <<< set (_login <<< _password) (targetValue ev)
       _ -> noEffects

rememberMe :: State -> EffModel State Query StateEff
rememberMe state = noEffects $ state { rememberMe = not state.rememberMe }

showLinks :: String -> State -> EffModel State Query StateEff
showLinks token state =
  { state:
    state { status = good "Logged in successfully"
          , token = Just token
        }
  , effects: [remember state.rememberMe token]
}

type Storage eff = (webStorage :: WebStorage | eff)

remember :: forall eff. Boolean -> String -> Aff (Storage eff) (Maybe Query)
remember = if _ then remember' localStorage else remember' sessionStorage

remember'
  :: forall eff s. Storage s => s
  -> String -> Aff (Storage eff) (Maybe Query)
remember' storage token =
  (Just $ Submit Links) <$ liftEff (setItem storage "token" token)

isLoaded :: IsLoaded -> State -> EffModel State Query StateEff
isLoaded Loaded st@{token: Just _}
  = onlyEffects st [purem $ Submit Links]
isLoaded _ state = noEffects state
