module Data.Proposition where

import Prelude
import Data.List as List
import Data.List (List)

data Lit a
  = Atom a
  | Not a

newtype Conj a = Conj (List (Lit a))

newtype Disj a = Disj (List (Conj a))

type Proposition = Disj

emptyProp :: forall a. Proposition a
emptyProp = Disj List.Nil

valuation
  :: forall c e
   . (c -> e -> Boolean)
  -> c
  -> Disj e
  -> Boolean
valuation interp model (Disj xs) =
  List.any (model `satisfiesConjVia` interp) xs

satisfiesConjVia
  :: forall c e
   . c
  -> (c -> e -> Boolean)
  -> Conj e
  -> Boolean
satisfiesConjVia model interp (Conj xs) =
  List.all (model `satisfiesLitVia` interp) xs

satisfiesLitVia
  :: forall c e
   . c
  -> (c -> e -> Boolean)
  -> Lit e
  -> Boolean
satisfiesLitVia model interp =
  case _ of
       Atom a -> model `interp` a
       Not a -> not $ model `interp` a
