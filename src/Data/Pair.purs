module Data.Pair
  ( Pair
  , module Tuple
  , pair ) where

import Data.Tuple (Tuple(..))
import Data.Tuple as Tuple

type Pair a = Tuple a a

pair :: forall a. a -> Pair a
pair x = Tuple x x
