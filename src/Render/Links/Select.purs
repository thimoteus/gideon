module Render.Links.Select where

import Prelude hiding (id)
import Data.Proposition (Proposition, Conj(..), Disj(..), Lit(..))
import Util (intersperse)

import Data.Foldable (intercalate)
import Data.List (List, fold)
import Pux.DOM.Events (onChange, onClick)
import Pux.DOM.HTML (HTML)
import Text.Smolder.HTML (input, label, span)
import Text.Smolder.HTML.Attributes (checked, for, id, type', value, className)
import Text.Smolder.Markup (text, (!), (!?), (#!))
import Types.Query (Query(..))
import Types.State (State)

renderSelect :: State -> HTML Query
renderSelect state = do
  renderShowFilter state
  renderRemoveFilter
  renderWriteLit state
  renderSelectLit state.filter.negate

renderWriteLit :: State -> HTML Query
renderWriteLit state = do
  label ! for "lit" $ text "Tag:"
  input ! type' "text" ! id "lit" #! onChange ChangeLit ! value state.filter.currTag

renderSelectLit :: Boolean -> HTML Query
renderSelectLit neg = do
  (input !? neg)
    (checked "checked")
    ! type' "checkbox"
    ! id "negateLit"
    #! onChange (const NegateFilter)
  label ! for "negateLit" $ text "exclude tag"

renderShowFilter :: State -> HTML Query
renderShowFilter state@{filter: {prop}} =
  fold $ intersperse (text "or") $ map text $ renderProp prop

renderProp :: forall a. Show a => Proposition a -> List String
renderProp (Disj xs) = map renderConj xs
  where
  renderConj (Conj ys) = intercalate " and " (map renderLit ys)
  renderLit (Atom a) = show a
  renderLit (Not a) = "not " <> show a

renderRemoveFilter :: HTML Query
renderRemoveFilter =
  span ! className "removeFilter" #! onClick (const RemoveFilter) $ text "x"
