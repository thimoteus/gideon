module Render.Auth where

import Prelude hiding (div, id)

import Types.State (State, AuthState, _username, _email, _password, _register, _login)
import Types.Query (Query(..), Component(..))

import Data.Lens ((^.))
import Pux.DOM.HTML (HTML)
import Text.Smolder.HTML (input, button, div, label, h2)
import Text.Smolder.Markup ((!?), (#!), (!), text)
import Text.Smolder.HTML.Attributes (type', value, placeholder, checked, id, for, className)
import Pux.DOM.Events (DOMEvent, onChange, onClick, onKeyUp)

authInput
  :: forall a. String
  -> String
  -> String
  -> (DOMEvent -> a)
  -> (DOMEvent -> a)
  -> HTML a
authInput tp lns ph change keyup
  = input
  ! type' tp
  ! value lns
  ! placeholder ph
  #! onChange change
  #! onKeyUp keyup

renderRegister :: AuthState -> HTML Query
renderRegister authstate = div ! className "register" $ do
  h2 $ text "Register"
  authInput
    "text"
    (authstate ^. _email)
    "email"
    (EmailChange Register)
    (KeyPress Register)
  authInput
    "text"
    (authstate ^. _username)
    "username"
    NameChange
    (KeyPress Register)
  authInput
    "password"
    (authstate ^. _password)
    "password"
    (PasswordChange Register)
    (KeyPress Register)
  button ! type' "button" #! onClick (\_ -> Submit Register) $ text "Register"

renderLogin :: Boolean -> AuthState -> HTML Query
renderLogin isChecked authstate = div ! className "login" $ do
  h2 $ text "Log in"
  authInput
    "text"
    (authstate ^. _email)
    "email"
    (EmailChange Login)
    (KeyPress Login)
  authInput
    "password"
    (authstate ^. _password)
    "password"
    (PasswordChange Login)
    (KeyPress Login)
  (input !? isChecked)
    (checked "checked")
    ! type' "checkbox"
    ! id checkboxID
    #! onChange (const RememberMe)
  label ! for checkboxID $ text "Remember me"
  button ! type' "button" #! onClick (const $ Submit Login) $ text "Login"

checkboxID :: String
checkboxID = "rememberMe"

logout :: HTML Query
logout
  = div
  ! className "logout"
  #! onClick (const $ Submit Logout)
  $ text "logout"

renderAuth :: State -> HTML Query
renderAuth state = div ! className "authenticate" $ do
  renderLogin state.rememberMe $ state ^. _login
  renderRegister $ state ^. _register
