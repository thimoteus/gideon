module Render.App where

import Prelude hiding (div)
import Render.Auth (logout, renderAuth)
import Render.Links (renderLinks)
import Types.State (State)
import Types.Query (Query)
import Util (StatusText(..), Status(..))

import Data.Maybe (Maybe(..))
import Pux.DOM.HTML (HTML)
import Text.Smolder.HTML (div, p, span)
import Text.Smolder.Markup (text, (!))
import Text.Smolder.HTML.Attributes (className)

renderApp :: State -> HTML Query
renderApp s@{token: Just _} = template {header: logout, body: renderLinks s} s
renderApp s = template {header: pure unit, body: renderLoggedIn s} s

template :: { header :: HTML Query, body :: HTML Query } -> State -> HTML Query
template {header, body} state = div $ do
  div ! className "header" $ do
    div ! className "status" $ status state.status
    header
  div ! className "body" $ body

renderLoggedIn :: State -> HTML Query
renderLoggedIn state = do
  div ! className "intro" $ do
    p $ text "Waverider is a tool made for saving, tagging and archiving links."
  renderAuth state

status :: forall a. StatusText -> HTML a
status (StatusText Good str) = span ! className "good" $ text str
status (StatusText Medium str) = span ! className "medium" $ text str
status (StatusText Poor str) = span ! className "poor" $ text str
