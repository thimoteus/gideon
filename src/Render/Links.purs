module Render.Links where

import Prelude hiding (div)
import Types.Links (Link, _url, _tags, _date, _currTag, _archive)
import Types.Query (Query(..), Component(..))
import Types.State (State)
import Util (Parity, tagParity, showDateTime, httprefix)

import Data.DateTime (DateTime)
import Data.Foldable (intercalate)
import Data.Lens (view, (^.))
import Data.Maybe (Maybe, maybe)
import Data.Traversable (traverse_)
import Data.Tuple (Tuple(..))
import Pux.DOM.HTML (HTML)
import Pux.DOM.Events (onChange, onKeyUp, onClick)
import Text.Smolder.HTML (div, button, input, span, a, h2)
import Text.Smolder.Markup (text, (#!), (!))
import Text.Smolder.HTML.Attributes (target, type', value, placeholder, className, href)

renderLinks :: State -> HTML Query
renderLinks state = div ! className "renderLinks" $ do
  newLinkForm state.newLink
  div ! className "links" $ do
    h2 $ text "Saved links"
    displayAllLinks state.links

displayAllLinks :: Array Link -> HTML Query
displayAllLinks [] = div ! className "noLinks" $
  text "There's nothing here! Try adding a link using the form above."
displayAllLinks xs = traverse_ linkToHTML $ tagParity xs

linkToHTML :: forall a. (Tuple Link Parity) -> HTML a
linkToHTML (Tuple l parity) = div ! className ("link " <> show parity) $ do
  div $ urlToHTML $ l ^. _url
  div $ archiveToHTML $ l ^. _archive
  div $ dateToHTML $ l ^. _date
  div $ tagsToHTML $ l ^. _tags

urlToHTML :: forall a. String -> HTML a
urlToHTML url = a ! href (httprefix url) ! target "_blank" $ text url

archiveToHTML :: forall a. String -> HTML a
archiveToHTML "" = text "No archive exists yet"
archiveToHTML arc = a ! href (arc) ! target "_blank" $ text "Archive"

dateToHTML :: forall a. Maybe DateTime -> HTML a
dateToHTML date = text $ "Saved on " <> maybe "just now" showDateTime date

tagsToHTML :: forall a. Array String -> HTML a
tagsToHTML = traverse_ \ tag -> span ! className "tag" $ text tag

newLinkForm :: Maybe Link -> HTML Query
newLinkForm newLink
  = div ! className "newLinkForm" $ do
    h2 $ text "Submit a new link"
    input ! type' "text"
          ! value (getNewLinkUrl newLink)
          ! placeholder "url"
          #! onChange UrlChange
          #! onKeyUp (KeyPress NewLink)
    input ! type' "text"
          ! value (getNewLinkTag newLink)
          ! placeholder "tags, separated by commas"
          #! onChange TagChange
          #! onKeyUp (KeyPress NLTag)
    span $ text $ getNewLinkTagArray newLink
    button ! type' "button" #! onClick (const $ Submit NewLink) $ text "Submit"

getNewLinkUrl :: Maybe Link -> String
getNewLinkUrl = maybe "" $ view _url

getNewLinkTag :: Maybe Link -> String
getNewLinkTag = maybe "" $ view _currTag

getNewLinkTagArray :: Maybe Link -> String
getNewLinkTagArray = maybe "" $ intercalate ", " <<< view _tags
