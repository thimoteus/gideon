module Main where

import Prelude
import Render.App (renderApp)
import Types.State (AppEff, initState)
import Types.Query (Query(..))
import Update.App (foldp)
import Loaded (match)

import Browser.WebStorage (getItem, localStorage, sessionStorage)
import Control.Monad.Eff (Eff)
import Data.Maybe (isJust)
import DOM.HTML (window)
import Pux (start)
import Pux.DOM.History (sampleURL)
import Pux.Renderer.React (renderToDOM)

main :: Eff AppEff Unit
main = do
  locTok <- getItem localStorage "token"
  sessTok <- getItem sessionStorage "token"
  urlSignal <- sampleURL =<< window
  app <- start
    { initialState: initState if isJust sessTok
      then {token: sessTok, rememberMe: false}
      else {token: locTok, rememberMe: true}
    , foldp
    , view: renderApp
    , inputs: [IsLoaded <<< match <$> urlSignal]
  }

  renderToDOM "#app" app.markup app.input
