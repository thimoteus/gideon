module Util where

import Prelude

import Control.Monad.Except (runExcept)
import Data.Array (intercalate, foldl)
import Data.DateTime (DateTime(..), year, month, day, hour, minute)
import Data.Either (either)
import Data.Enum (fromEnum)
import Data.Tuple (Tuple(..))
import Data.Maybe (Maybe)
import Data.String (take)
import Data.List as List
import Pux.DOM.Events (DOMEvent)
import DOM.Event.KeyboardEvent (eventToKeyboardEvent, key)

purem :: forall f a. Applicative f => a -> f (Maybe a)
purem = pure <<< pure

getKeycode :: DOMEvent -> String
getKeycode = either (const "") key <<< runExcept <<< eventToKeyboardEvent

leadingZero :: Int -> String
leadingZero n
  | n < 10 && (-10) < n = "0" <> show n
  | otherwise = show n

showDateTime :: DateTime -> String
showDateTime (DateTime a b) = f a <> " at " <> g b
  where
    f x =
      let y = year x
          m = month x
          d = day x
       in intercalate " " [show m, show (fromEnum d) <> ",", show (fromEnum y)]
    g y =
      let h = hour y
          m = minute y
       in leadingZero (fromEnum h) <> ":" <> leadingZero (fromEnum m)

data Parity = Even | Odd

instance showParity :: Show Parity where
  show Even = "even"
  show _ = "odd"

switch :: Parity -> Parity
switch Even = Odd
switch _ = Even

tagParity :: forall a. Array a -> Array (Tuple a Parity)
tagParity = _.arr <<< foldl f {latest: Even, arr: []}
  where
    f {latest, arr} el = {latest: switch latest, arr: arr <> [Tuple el latest] }

httprefix :: String -> String
httprefix url
  | take 4 url == "http" = url
  | otherwise = "http://" <> url

data Status
  = Good
  | Medium
  | Poor

data StatusText = StatusText Status String

good :: String -> StatusText
good = StatusText Good

medium :: String -> StatusText
medium = StatusText Medium

poor :: String -> StatusText
poor = StatusText Poor

intersperse :: forall a. a -> List.List a -> List.List a
intersperse _ List.Nil = List.Nil
intersperse _ (List.Cons x List.Nil) = List.Cons x List.Nil
intersperse x (List.Cons y ys) = List.Cons y $ List.Cons x $ intersperse x ys
