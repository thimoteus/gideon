module Request where

import Prelude
import Types.State (State)

import Data.Either (Either(..))
import Data.Maybe (Maybe(..), maybe)
import Data.HTTP.Method (Method(..))
import Network.HTTP.Affjax (URL, AffjaxRequest, Affjax, defaultRequest, affjax)
import Network.HTTP.Affjax as Affjax
import Network.HTTP.Affjax.Response (class Respondable)
import Network.HTTP.Affjax.Request (class Requestable)
import Network.HTTP.RequestHeader (RequestHeader(..))

authRequest :: String -> AffjaxRequest Unit
authRequest token =
  defaultRequest
    { headers = [RequestHeader "Authorization" $ "Bearer " <> token]
  }

getAsUser :: forall a eff. Respondable a => String -> URL -> Affjax eff a
getAsUser token u = affjax (authRequest token) {url = u}

postAsUser :: forall a eff b. (Requestable a, Respondable b) => String -> URL -> a -> Affjax eff b
postAsUser token u c = affjax (authRequest token) {method = Left POST, url = u, content = Just c}

get :: forall a eff. Respondable a => State -> URL -> Affjax eff a
get state = maybe Affjax.get getAsUser state.token

post :: forall a eff b. (Requestable a, Respondable b) => State -> URL -> a -> Affjax eff b
post state = maybe Affjax.post postAsUser state.token
